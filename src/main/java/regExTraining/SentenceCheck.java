package regExTraining;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceCheck {
    private static String sentence = "Hello, my name is Bobbygale; I have the animal," +
            " do you have an animal; my dog sleeps in the house; what do you think about it.";

    public static void main( final String[] args) {
        Logger logger = LogManager.getLogger(SentenceCheck.class);
        // first task
        Pattern p = Pattern.compile("^[A-Z].*\\.$");
        Matcher m = p.matcher(sentence);
        while(m.find()){
            logger.info(m.start() + " " + m.group());
        }
        // second task
        String[] str = sentence.split("you|the");
        Arrays.stream(str).forEach(logger::info);
        // third task
        logger.info(sentence.replaceAll("[aAeEiIoOuU]","_"));
    }
}
