package stringUtils_InternationalizationTraining;

import java.util.Arrays;

public class StringUtils {
    private Object[] obj;
    public StringUtils(Object ... obj){
        this.obj = obj;
    }
    @Override
    public String toString(){
        return Arrays.stream(obj).map(x -> x.toString()).reduce((x,y) -> x + y).get();

    }
}
