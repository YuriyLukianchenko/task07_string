package stringUtils_InternationalizationTraining;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;


public class MenuInternational {
    private Logger logger = LogManager.getLogger(MenuInternational.class);
    private Scanner scanner = new Scanner(System.in);
    private ResourceBundle bundleEnglish = ResourceBundle.getBundle("international",
            new Locale("en","EN"));
    private ResourceBundle bundleUkraine = ResourceBundle.getBundle("international",
            new Locale("uk","UA"));
    private ResourceBundle bundle = bundleEnglish;
    public static void main( final String[] args) {
        MenuInternational menu = new MenuInternational();
        menu.showMenu();
        menu.readInput();

    }
    void showMenu(){
        logger.info(bundle.getString("option0"));
        logger.info(bundle.getString("option1"));
        logger.info(bundle.getString("option2"));
        logger.info(bundle.getString("option3"));
        logger.info(bundle.getString("option4"));
    }
    void readInput(){

        label:
        while (true){
            String str = scanner.nextLine();
            switch(str){
                case "1":
                    StringUtils StrUti = new StringUtils(1, 2, "sdfe");
                    logger.info(StrUti.toString());
                    break;
                case "2":
                    bundle = bundleEnglish;
                    showMenu();
                    break;
                case "3":
                    bundle = bundleUkraine;
                    showMenu();
                    break;
                case "4":
                    break label;
                default:
            }
        }
    }

}
