/**
 * Package for view element of MVC pattern.
 */
package bigRegExTaskApp.view;

import bigRegExTaskApp.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Scanner;
import java.io.File;

/**
 * Class contains menu and common methods of view part.
 */
public class View {
    /**
     * Logger from log4j2 lib.
     */
    private Logger logger;
    /**
     * Scanner for reading input information from console.
     */
    private Scanner scanner;
    /**
     * Controller, element of MVC pattern.
     */
    private Controller controller;

    /**
     * Constructor with initialization of private fields.
     */
    public View(){
        logger = LogManager.getLogger(View.class);
        scanner = new Scanner(System.in);
        controller = new Controller();
    }
    /**
     * Method where all step of program is performed.
     */
    public void start(){
        logger.trace("starting of program");
        showInitInfo();
        showOptions();
        optionChoice();
    }
    /**
     * Show initial information.
     */
    private void showInitInfo(){
        logger.info("This is the program allows performs different actions with text");
    }
    /**
     * Show options of menu.
     */
    private void showOptions(){
        logger.info("1 - download text from file");
        logger.info("2 - perform action");
        logger.info("q - quit");
    }
    private void optionChoice(){
        label:
        while(true){
            String str = scanner.nextLine();
            switch(str) {
                case "1":
                    try (Scanner scanner = new Scanner( new File("text.txt"), "UTF-8" )) {
                        String text = scanner.useDelimiter("\\A").next();
                        controller.loadFile(text);
                        logger.info("-Text was uploaded successful-");
                    }
                    catch(IOException ex){
                        logger.info("-Problem with text uploading-");
                        logger.info(ex.getMessage());
                    }
                    break;
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                case "10":
                case "11":
                case "12":
                case "13":
                case "14":
                case "15":
                case "16":
                case "17":
                    controller.performAction(str);
                    break;
                case "q":
                    scanner.close();
                    break label;
                default:
                    scanner.close();
            }
        }
    }
}
