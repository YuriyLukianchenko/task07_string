/**
 * package with full program for text regex testing
 *
 * @Author Yura
 * @Version 1.0
 */
package bigRegExTaskApp;

import bigRegExTaskApp.view.View;

/**
 * Class with entry point.
 */
public class Application {
    /**
     * Default constructor.
     */
    public void Application(){
    }
    /**
     * Main method, entry point.
     * @param args arguments of psvm.
     */
    public static void main( final String[] args) {
        View view = new View();
        view.start();
    }

}
