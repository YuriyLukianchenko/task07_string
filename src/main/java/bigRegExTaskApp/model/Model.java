/**
 * Package for model part of MVC pattern.
 */
package bigRegExTaskApp.model;
/**
 * Class contains text downloaded from file and regex methods.
 */
public class Model {
    /**
     * Field where all information about text is.
     */
    private Topic topic;
    /**
     * Text downloaded from file.
     */
    private String text;
    /**
     * Getter fo text field.
     * @return text downloaded from file.
     */
    public String getText() {
        return text;
    }
    /**
     * Setter for text field.
     * @param text
     */
    public void setText(String text) {
        this.text = text;
        topic = new Topic();
    }
    /**
     * Getter for topic.
     * @return topic (text from file).
     */
    public Topic getTopic() {
        return topic;
    }
    /**
     * Setter for topic.
     * @param topic topic (information about text from file and the text)
     */
    public void setTopic(Topic topic) {
        this.topic = topic;
    }
    /**
     * Constructor of model.
     */
    public Model(){
        topic = new Topic();
    }
    /**
     * Method finds maximal count of sentences with identical words.
     */
    public void maxCountOfSentencesWithIdenticalWords(){
        topic.countOfSentences(text);
        topic.showStatistics();

    }
}
