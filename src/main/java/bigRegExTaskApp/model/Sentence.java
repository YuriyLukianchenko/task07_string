/**
 * Package of model part of MVC pattern.
 */
package bigRegExTaskApp.model;

import java.util.List;

/**
 * Class where is stored information about sentence
 * and its parts (words).
 */
public class Sentence {
    private String sentence;
    private List<Word> words;

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    public Sentence(String str){
        this.sentence = str;
    }
}
