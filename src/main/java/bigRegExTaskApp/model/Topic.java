/**
 * Package of model part of MVC pattern.
 */
package bigRegExTaskApp.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class where is stored information about text
 * and its parts (sentences).
 */
public class Topic {
    /**
     * Logger.
     */
    private Logger logger;
    /**
     * List of sentences.
     */
    private List<Sentence> sentences;
    /**
     * Count of sentences in text.
     */
    private int sentenceCount;
    /**
     * Count of words in text.
     */
    private int wordCount;
    /**
     * Count of letters in text.
     */
    private int letterCount;

    /**
     * Constructor with initialization of logger and sentences.
     */
    public Topic(){
        logger = LogManager.getLogger(Topic.class);
        sentences = new ArrayList<>();
        sentenceCount = 0;
        wordCount = 0;
        letterCount = 0;
    }

    /**
     * Method wich calculates number of sentences in text.
     * @param text text downloaded from file.
     * @return number of sentences in text.
     */
    public int countOfSentences(String text){
        sentenceCount = 0;
        Pattern p = Pattern.compile("[.!?][ \\u000d]");
        Matcher m = p.matcher(text);

        int previousIndex = 0;
        while(m.find()){
            sentenceCount += 1;
            sentences.add(new Sentence(text.substring(previousIndex, m.start()+1)));
            previousIndex = m.end();
            //logger.trace(m.start() + " " + m.group());
        }
        return sentenceCount;
    }

    /**
     * Show statistics of the text.
     */
    public void showStatistics(){
        logger.info("number of sentences = " + sentenceCount);
        logger.info("number of words = " + wordCount);
        logger.info("number of letters = " + letterCount + "\n");
        sentences.stream().forEach(x -> logger.info(x.getSentence()+ "\n"));
    }
}
