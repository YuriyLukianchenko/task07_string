/**
 * Package with controller part of MVC pattern.
 */
package bigRegExTaskApp.controller;


import bigRegExTaskApp.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Controller class.
 */
public class Controller {
    /**
     * Here store inputted text and methods for work with the text.
     */
    private Model model;
    /**
     *Logger for logging.
     */
    private Logger logger;
    /**
     * Constructor with initialization of private fields.
     */
    public Controller(){
         logger = LogManager.getLogger(Controller.class);
         model = new Model();
     }

    /**
     * Getter for model.
     * @return model.
     */
    public Model getModel() {
        return model;
    }
    /**
     * Getter for logger.
     * @return logger.
     */
    public Logger getLogger() {
        return logger;
    }
    /**
     *Method which performs menu actions
     */
    public void performAction(String str){
        switch(str){
            case "2":
                model.maxCountOfSentencesWithIdenticalWords();
                break;
            default:
        }
    }
    /**
     * Load text from file in model.text.
     * @param text text downloaded from file.
     */
    public void loadFile(String text){
        model.setText(text);
    }
    /**
     * Show text from file.
     * @return text downloaded from file.
     */
    public String showTextFromFile(){
        return model.getText();
    }
}
